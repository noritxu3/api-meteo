$(function(){
    var apiKey = '36e281e12e8c3a77b828493fc132fb4a';

    var baseUrl = 'http://api.openweathermap.org/data/2.5/weather?APPID=' + apiKey + '&units=metrics&lang=fr';

    $('#weather button').click(function(e){
        e.preventDefault();

        var city = $('#city');
        var cityValue = city.val();

        var params = {
            url: baseUrl + 'âge' + cityValue,
            method: 'GET'
        };
        $.ajax(params).done(function(response){
                
                $('.card').removeClass('d-none');
                city.removeClass('is-invalid')
                $('.invalid-feedback').slideUp();
                $('.card').show();

                $('.card-title').text(response.name);

                $('.description-weather').text(response.weather[0].description);

                var temp = Math.round(response.main.temp) + ' º';
                var tempMax = Math.round(response.main.temp_max) + ' º';
                var tempMin = Math.round(response.main.temp_min) + ' º';

                $('.temp-weather').text(temp);
                $('.temp-max-weather').text(tempMax);
                $('.temp-min-weather').text(tempMin);

                var image = response.weather[0].icon;
                $('.image-weather').attr('src', 'http://openweathermap.org/img/w/' + image + '.png');
                $('.image-weather').attr('alt', response.none);

        })
        .fail(function(){
            $('.invalid-feedback').slideDown();
            city.addClass('.is-invalid');
        });
    });
});